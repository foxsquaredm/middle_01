#include "Middle_01.h"

#include <iostream>
#include <cmath>

class Vector
{
public:

	Vector()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	explicit Vector(float num)		//if you don't want to use: Vector v1 = 1;
	{
		x = num;
		y = num;
		z = num;
	}

	//Vector(float num) = delete;		//if you want to forbidden initialization with one parameter 

	Vector(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;

		//Info = new std::string("Important!");
	}

	Vector(const Vector& other)
	{
		std::cout << "\n Copy constructor \n";
		x = other.x;
		y = other.y;
		z = other.z;

		//Info = new std::string(*(other.Info));
	}

	~Vector()
	{
		//delete Info;
		//std::cout << "destructor \n";
	}

	Vector operator=(Vector& other)
	{
		std::cout << "\n Assignment constructor \n";
		x = other.x;
		y = other.y;
		z = other.z;

		if (other.Info)
		{
			//Info = other.Info;						//shallow copying
			if (Info) delete Info;
			Info = new std::string(*(other.Info));		//deep copying
		}

		return (*this);
	}

	operator float()
	{
		return sqrt(x * x + y * y + z * z);
	}

	//MyComment: task 1
	friend Vector operator*(const Vector& a, const int b);

	friend Vector operator+(const Vector& a, const Vector& b);

	//MyComment: task 2
	friend Vector operator-(const Vector& a, const Vector& b);

	friend std::ostream& operator<<(std::ostream& out, const Vector& v);

	//MyComment: task 3
	friend std::istream& operator>>(std::istream& in, Vector& v);

	friend bool operator>(const Vector& a, const Vector& b);

	float operator[](int index)
	{
		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;

		default:
			std::cout << "Index error";
			return 0;
			break;
		}
	}


//private:
	float x;
	float y;
	float z;

private:
	std::string* Info;
};

//MyComment: task 1
//Vector operator*(const Vector& a, const float b)
Vector operator*(const Vector& a, const int b)
{
	return Vector(a.x * (float)b, a.y * (float)b, a.z * (float)b);
}

Vector operator+(const Vector& a, const Vector& b)
{
	//Vector Result(a.x + b.x, a.y + b.y, a.z + b.z);
	return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

//MyComment: task 2
Vector operator-(const Vector& a, const Vector& b)
{
	return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

std::ostream& operator<<(std::ostream& out, const Vector& v)
{
	out << ' ' << v.x << ' ' << v.y << ' ' << v.z;
	return out;
}

//MyComment: task 3
std::istream& operator>>(std::istream& in, Vector& v)
{
	std::cout << "x: " << '\n';
	in >> v.x;
	std::cout << "y: " << '\n';
	in >> v.y;
	std::cout << "z: " << '\n';
	in >> v.z;

	return in;
}

bool operator>(const Vector& a, const Vector& b)
{
	return false;
}

class A
{
public:
	A(int a)
	{
		std::cout << "int constructor " << '\n';
		test = a;
	}
	
	A(char a) = delete;

private:
	int test;
};


class PR
{
public:

	PR(int N)
	{
		for (int i = 0; i < N; i++)
		{
			DynArray[i] = new int[i + 1];
		}

		for (int i = 0; i < N; i++)
		{
			std::cout << "Enter numbers for " << i + 1 << ":" << "\n";
			for (int j = 0; j < i + 1; j++)
			{
				std::cin >> DynArray[i][j];
			}
		}

		std::cout << "Resultat:" << "\n";

		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < i + 1; j++)
			{
				std::cout << DynArray[i][j] << " ";
			}
			std::cout << "\n";
		}
		std::cout << "\n";
	}

	~PR()
	{
		std::cout << "\n Delete constructor \n";
		//delete DynArray;
		if (DynArray)
		{
			int CountMassiv = sizeof(DynArray) / sizeof(DynArray[0]);
			for (int i = 0; i < CountMassiv; i++)
			{
				delete[] DynArray[i];
			}
			delete[] DynArray;
		}
	}

	PR operator=(PR& other)
	{
		std::cout << "\n Assignment constructor \n";

		//if (this == &other)
		//	return *this;

		////int i = 0;
		////while (DynArray[i])
		////{
		////	i++;
		////	DynArray[i] = other.DynArray[i];
		////}

		//if (other.DynArray)
		//{
		//	if (DynArray) delete DynArray;
		//	DynArray = new int** DynArray(*(other.DynArray));
		//	//Info = new std::string(*(other.Info));

		//	int CountMassiv = sizeof(DynArray) / sizeof(DynArray[0]);
		//	for (int i = 0; i < CountMassiv; i++)
		//	{
		//		DynArray[i] = other.DynArray[i];
		//	}
		//}

		return (*this);
	}

private:
	int** DynArray;

};

class MyTestClass
{
public:
	//MyTestClass(int Carray_x, int Carray_y)
	MyTestClass()
	{
		array_x = 0;
		array_y = 0;
		InitArray();
	}
//	~MyTestClass();
	
	//void InitArray(int Carray_x, int Carray_y)
	void InitArray()
	{
		//Info = new std::string("Important!");
		DynamicArray = new int* [array_x];

		for (int i = 0; i < array_x; i++)
		{
			DynamicArray[i] = new int[array_y];
		}
		std::cout << "Init Array\n";
	}

	void FillArray()
	{
		for (int i = 0; i < array_x; i++)
		{
			std::cout << "Enter numbers for " << i + 1 << ":" << "\n";
			for (int j = 0; j < array_y; j++)
			{
				std::cin >> DynamicArray[i][j];
			}
		}
	}

	void ShowArray()
	{
		std::cout << "Resultat:" << "\n";
		for (int i = 0; i < array_x; i++)
		{
			for (int j = 0; j < array_y; j++)
			{
				std::cout << DynamicArray[i][j] << " ";
			}
			std::cout << "\n";
		}
		std::cout << "\n";
	}

	void DeleteArray()
	{
		if (DynamicArray)
		{
			for (int i = 0; i < array_x; i++)
			{
				delete[] DynamicArray[i];
			}
			delete[] DynamicArray;
		}
	}

	void CopyArray(int** other)
	{
		for (int i = 0; i < array_x; i++)
		{
			for (int w = 0; w < array_y; w++)
			{
				DynamicArray[i][w] = other[i][w];
			}
		}
	}

	MyTestClass& operator=(const MyTestClass& other)
	{
 		array_x = other.array_x;
		array_y = other.array_y;
		if (other.Info)
		{
			if (Info) delete Info;
			Info = new std::string(*(other.Info));
		}
		if (other.DynamicArray)
		{
			DeleteArray();
			//InitArray(array_x, array_y);
			InitArray();
			CopyArray(other.DynamicArray);
		}
		std::cout << "Init from = operator \n";
		return (*this);
	}



private:
	int** DynamicArray;
	int array_x;
	int array_y;
	std::string* Info;
};


int main()
{
	/////////////////////////////////////////////////////////////////////
	//Middle_01

	//float x = 1;
	//float y = 2;
	//std::cout << x + y;

	//Vector v1(0, 1, 2);
	//Vector v2(3, 4, 5);
	//std::cout << v1 + v2 << '\n';
	//std::cout << v2[1];

	//Vector v1(0, 1, 2);
	//Vector v2(3, 4, 5);
	//Vector v3;
	//v3 = v1 + v2;

	//std::cout << v3 << '\n';
	//std::cout << "v3 - lenght " << float(v3) << '\n';
	//std::cout << "v3 - lenght " << static_cast<float>(v3) << '\n';

	//std::cout << "==================================================" << '\n';

	////MyComment: task 1
	//std::cout << '\n';
	//std::cout << v1 * 3 << '\n';

	////MyComment: task 2
	//std::cout << '\n';
	//Vector v4;
	//v4 = v3 - v1;
	//std::cout << "v4 - lenght " << v4 << '\n';

	////MyComment: task 3
	//Vector v5;
	//std::cout << '\n';
	//std::cout << "Enter new vector " << '\n';
	//std::cin >> v5;
	//std::cout << "New vector: " << v5 << '\n';

	/////////////////////////////////////////////////////////////////////
	//Middle_02
	 
	//Vector v1{ 0, 1, 2 };
	////Vector v2(3, 4, 5);
	//Vector v3 = v1;	//copy
	////Vector v3 (v1);		//copy, default

	//Vector v1(1);
	//std::cout << "vector v1: " << v1 << '\n';

	//char testChar = 'k';
	//A a(1);
	////A a(testChar);		//if we use: delete

	//Vector v1(1, 1, 1);
	//Vector v2(2, 2, 2);
	//std::cout << v2 << '\n';

	//v2 = v1;		//we use equals, after the constructor for v2, so copy constructor won't work, but the assignment constructor
	//std::cout << v2 << '\n';
	

	//MyComment: task

	//PR M1(5);
	////PR CreatDynMassiv(5);
	//std::cout << "\n new massiv =============== \n";
	//PR M2(5);
	////M2 = M1;

	//MyTestClass* Mass1 = new MyTestClass(4, 5);
	////Mass1->InitArray(4, 5);
	//Mass1->FillArray();
	//Mass1->ShowArray();

	//MyTestClass* Mass2;
	//Mass2 = Mass1;
	//Mass2->ShowArray();

	MyTestClass Mass2;
	MyTestClass Mass1(2, 3);


	Mass2 = Mass1;
}